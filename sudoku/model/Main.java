package sudoku.model;

/**
 * Main class is where the Sudoku game will run, The only supported and tested
 * sizes in this version 1.o are (4x4) and (9x9) grids.
 * 
 * @author Felipe Lopez
 * @version 1.0
 * 
 */

public class Main {
	private ConsoleUI ui = new ConsoleUI(); // player (user interface)
	private Board board; // 2-d array representing a Sudoku board

	public static void main(String[] args) {
		// initialize Sudoku game
		new Main().play();

	}
	

	/**
	 * Uses a conditional while loop to emulate a Sudoku game. The following
	 * steps are a description of how the game is run
	 * 
	 * 1. Print out a welcome message *USER CAN ENTER -1 FROM HERE ON AT ANY
	 * TIME TO END THE GAME 2. Ask user for the size of board to be played 3.
	 * Draw the empty board 4. Request and receive input for x,y coordinates and
	 * Value to be placed. 5. Check that user input is valid. 6. Draw the
	 * updated board 5. Continue steps 4-6 until there are no more values in the
	 * board to be placed. 7. End game.
	 */

	private void play() {
		ui.welcome();
		int size = ui.getUserInput();
		while(size < 0){
			ui.showMessage("Invalid size, please enter 4 or 9 "
					+ "if you would like to end the game first enter a valid board size"
					+ "then you will be given the option to end the game");
			
			size = ui.getUserInput();
		}
			
		
		board = new Board(size);
		ui.printBoard(board.getSize(), board.getSubset(), board.getBoard());


		while (!board.isSolved()) {

			// Check if player wants to continue playing game
			ui.showMessage(
					"Enter x-coordinate, then y-coordinate, then value." + "or Enter -1 at anytime to quit the game");
			int temp1 = ui.getUserInput(); // stores x-coordinate (column
											// number)
			if (temp1 == -1)
				break;
			int temp2 = ui.getUserInput(); // Store y-coordinate (row number)
			if (temp2 == -1)
				break;
			int temp3 = ui.getUserInput(); // Store value to be saved on the
											// sudoku board
			if (temp3 == -1)
				break;

			/**
			 * verify that range is within specifications verify row, column,
			 * and sub-grid of sudoku board
			 */
			if (board.verifyRange(temp1, temp2, temp3) && board.verify(temp1, temp2, temp3)) {
				board.placeNumberOnBoard(temp1, temp2, temp3); // valid input
				ui.printBoard(board.getSize(), board.getSubset(), board.getBoard());
			} else {
				ui.showMessage("Number is not valid for placement, please try again"); // invalid input
				ui.printBoard(board.getSize(), board.getSubset(), board.getBoard());

			}
		}
		if (board.isSolved()) {
			ui.showMessage("Congratulations, Your Sudoku is solved!"); // player
																		// has
																		// successfully
																		// completed
																		// the
																		// game
		} else {
			ui.showMessage("Exit command recieved, Goodbye!"); // player has chosen to end the game
										// before completing it.
		}
	}
}