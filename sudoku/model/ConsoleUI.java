package sudoku.model;


import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Provides a way to interact with the console by using various methods
 * of receiving and outputting input to and from the user.
 * @author Felipe Lopez
 * @version 1.0
 * 
 *
 */
public class ConsoleUI {
	private InputStream in;
	private PrintStream out;
	public Scanner scanner = new Scanner(System.in);

	
	public ConsoleUI(){
		this(System.in, System.out);
	}

	public ConsoleUI(InputStream in, PrintStream out){
		this.in = System.in;
		this.out = System.out;
	}
	
	/**
	 * Prints out a welcome message
	 */
	public void welcome(){
		out.println("Welcome to Sudoku! \n"
				+ "Enter the size of board you wish to play.. (4 or 9) \n");

	}
	
	/**
	 * Checks that user is only inputing Integer values
	 * If user enters an invalid input such as a String, 
	 * method will use a try catch block to catch the exception 
	 * and will then loop back to retrieve next user input.
	 * @return will only return Integer values
	 * 
	 */
	public  int getUserInput(){
		// verify user inputs are integer values
		boolean continueInput = true; // flag 
		int number = 0;
		do{
			try{
				number = scanner.nextInt();
				continueInput = false; // valid input received
			}
			catch (InputMismatchException ex){ // invalid input
				out.println("Try again. (valid inputs are only Integers)");
				scanner.nextLine();
			}
		} while(continueInput);
		return number;	
	}	
	
	/**
	 * Provides a way to output a general message to the user console
	 * Pass the "String" message in the arguments to generate the output 
	 * @param msg any String message 
	 */
	public void showMessage(String msg){
		out.println(msg);

	}

	public void printBoard(int size, int subset, int[][] board) {
		out.print("y/x"); // y and x coordinate reminder
		out.println();
		
		// prints out evenly spaced rows depending of board size
		for(int i = 0; i < size; i ++){ 
			if ( i%subset == 0){
				out.println();
			}
			
			// print out evenly spaced dividers depending on board size 
			for(int j = 0; j < size; j++){
				if (j%subset ==0 ){
					out.print(" | ");
					}
				
				// Print each current value
				out.print(board[i][j] + " ");
			}
			out.println();
		}
		
	}

}