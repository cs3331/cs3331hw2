package sudoku.model;

/**
 * This board class uses a 2-D integer array to store values in specified row and columns
 * Also provides method to print out this 2-D array in a (n*n) board fashion to 
 * resemble a Sudoku board and various methods to check for validity of user input
 * 
 * 
 * The following is a description of how the 2-d array is set up to resemble a sudoku board
 *        col0   col1  col2  col3
 * row 0   [ ]   [ ]   [ ]   [ ]
 * row 1   [ ]   [ ]   [ ]   [ ]
 * row 2   [ ]   [ ]   [ ]   [ ]
 * row 3   [ ]   [ ]   [ ]   [ ]
 * 
 * The following configuration uses a 2-d array 
 * The x-coordinate represents the horizontal axis of the board (columns)
 * The y-coordinate  represents by the vertical axis of the board (rows)
 *
 * This is the format used when declaring this 2-d array 
 * int[rows][columns]; 
 * Therefore, the ordered pair(x,y) will correspond to (column,row) 
 * @author Felipe Lopez
 * @version 1.0
 * 
 * NOTE: Empty Sudoku board values are represented by 0
 *
 */

public class Board{	
	private int size; // size of the Sudoku board (length or height)...
	private int[][] board; // Sudoku board
	private int subset; // sub-grid size

	// Empty constructor for Board object
	public Board(){
		setSize(0);
		board = null;
		setSubset(0);
		}

	/**
	 * Constructor for a Board object
	 *  Creates the size of the sub-grid of the sudoku board
	 *  Since only valid sizes of this puzzle are (4,9,16, etc..)
	 *  these valid sizes all are perfect squares 
	 *  Therefore, we can take the square root of the size of the board
	 *  and figure out the correct size of the sub-grid.
	 * @param size The size of the 2-d array to be created
	 */
	public Board(int size){
		this.setSize(size);
		this.board = new int[size][size];
		this.setSubset((int) Math.sqrt(size));
		}
	
	/**
	 * Verifies user input to prevent user from inputing a value which is 
	 * out of the range of the array
	 * 
	 * @param x the x coordinate of the current board
	 * @param y the y coordinate of the current board
	 * @param z the value at the current x and y coordinate of the 2-d array board
	 * @return true if number is within range, false if number is out of range
	 */
 
	public boolean verifyRange(int x, int y, int z){
		if( x > getSize() || y > getSize() || z > getSize()){
			return false;
		}
		if(x < 0 || y < 0 || z < 0){
			return false;
		}
		return true;
		}

	// verify column, row and sub-grid in one method
	public boolean verify(int x, int y, int z){
		
		//check whether z is unique in its row	
		for(int i = 0; i < board.length; i++){
			if(board[y][i] == z)
				return false;
			}
			
			
			//check whether z is unique in its column
			for(int j = 0; j< board.length; j++){
				if(board[j][x] == z)
					return false;
				}
				
			//check whether z is unique in its sub-grid
			for(int row = (x/getSubset())*getSubset(); row< (x/getSubset())*getSubset() +getSubset(); row++){
				for(int col = (y/getSubset())*getSubset(); col< (y/getSubset())*getSubset() +getSubset(); col++){
					if(this.board[col][row] == z){
						return false;
					}
				}
			}
			
		return true; // z is valid 
		}
	
	/**
	 * This method uses the inputs of the user to define the row and column of the 
	 * value being placed, and also that inputs value 
	 * @param x the column of the board (horizontal axis)
	 * @param y the row of the board (vertical axis)
	 * @param value number to be placed on the sudoku board
	 */
	public void placeNumberOnBoard(int x, int y, int value){
		board[y][x] = value;
		}
	
	/**
	 * Only checks if there are any values in the Sudoku board
	 * that are equal to zero.
	 * 
	 * Can not check if solution to the puzzle is valid, 
	 * Therefore this method must only be used if all inputs on the board
	 * have already been checked for validity.
	 * @return true if game is solved
	 */
	public boolean isSolved(){
		for(int i = 0; i<board.length;i++){
			for(int j = 0; j < board.length; j++){
				if(board[i][j]==0)
					return false;
				}
			}
		return true;
		}

	public int getSize() {
		return size;
		}

	public void setSize(int size) {
		this.size = size;
		}

	public int getSubset() {
		return subset;
		}

	public void setSubset(int subset) {
		this.subset = subset;
		}

	public int[][] getBoard() {
		return this.board;
		}
	}