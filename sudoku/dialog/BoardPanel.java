package sudoku.dialog;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import sudoku.model.Board;

/**
 * A special panel class to display a Sudoku board modeled by the
 * {@link sudoku.model.Board} class. You need to write code for
 * the paint() method.
 *
 * @see sudoku.model.Board
 * @author Yoonsik Cheon
 */
@SuppressWarnings("serial")
public class BoardPanel extends JPanel {

	public interface ClickListener {

		/** Callback to notify clicking of a square. 
		 * 
		 * @param x 0-based column index of the clicked square
		 * @param y 0-based row index of the clicked square
		 */
		void clicked(int x, int y);
	}


	/** Background color of the board. */
	private static final Color boardColor = new Color(247, 223, 150);

	/** Board to be displayed. */
	private Board board;

	/** Width and height of a square in pixels. */
	private int squareSize;

	/** Create a new board panel to display the given board. */
	public BoardPanel(Board board, ClickListener listener) {
		this.board = board;
		addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				int xy = locateSquaree(e.getX(), e.getY());
				if (xy >= 0) {
					listener.clicked(xy / 100, xy % 100);
					  System.out.println("Number of click: " + e.getClickCount());      
			    	  System.out.println("Mouse Clicked at X: " + e.getX()/30 + " - Y: " + e.getY()/30);
					 			
				}
				
			}
		});
	}

	/** Set the board to be displayed. */
	public void setBoard(Board board) {
		this.board = board;
	}

	/**
	 * Given a screen coordinate, return the indexes of the corresponding square
	 * or -1 if there is no square.
	 * The indexes are encoded and returned as x*100 + y, 
	 * where x and y are 0-based column/row indexes.
	 */
	private int locateSquaree(int x, int y) {
		if (x < 0 || x > board.getSize() * squareSize
				|| y < 0 || y > board.getSize() * squareSize) {
			return -1;
		}
		int xx = x / squareSize;
		int yy = y / squareSize;
		return xx * 100 + yy;
	}

	/** Draw the associated board. */
	@Override
	public void paint(Graphics g) {
		super.paint(g); 

		// determine the square size
		Dimension dim = getSize();
		squareSize = Math.min(dim.width, dim.height) / board.getSize();

		// draw background
		final Color oldColor = g.getColor();
		g.setColor(boardColor);
		g.fillRect(0, 0, squareSize * board.getSize(), squareSize * board.getSize());


		// WRITE YOUR CODE HERE ...
		    
		if(board.getSize() == 9) {
			
			g.setColor(Color.BLACK);
			for (int i = 0; i < 270; i +=30) {
				g.drawLine(0, i, 260, i);
				for(int j = 0; j < 260 ; j+=30) {
					g.drawLine(j, 0, j, 260);   
				}
			}
		}else if(board.getSize()==4){ 
			g.setColor(Color.BLACK);
			for (int i = 0; i < 270; i +=67.5) {
				g.drawLine(0, i, 260, i);
				for(int j = 0; j < 260 ; j+=67.5) {
					g.drawLine(j, 0, j, 260);   
				}
			}
		}
			  
		           
		   g.fillRect(30,30,30,30);
		 }
		 
		  }
